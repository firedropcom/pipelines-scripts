#!/bin/bash
set -eo pipefail
if [[ -f lint.Dockerfile ]]
then
	$(aws ecr get-login --no-include-email)
	docker build --build-arg ssh_key="$(cat /opt/atlassian/pipelines/agent/ssh/id_rsa)" -f lint.Dockerfile .
fi

echo Linting Complete
