#!/bin/bash
set -eo pipefail
if [[ -f test.Dockerfile ]]
then
	$(aws ecr get-login --no-include-email)
	docker build -f test.Dockerfile --build-arg ssh_key="$(cat /opt/atlassian/pipelines/agent/ssh/id_rsa)" .
else
	echo No Test Dockerfile
	exit 1
fi
